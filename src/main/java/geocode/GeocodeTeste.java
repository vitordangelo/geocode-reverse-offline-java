package geocode;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import geocode.ReverseGeoCode;

public class GeocodeTeste {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		ReverseGeoCode reverseGeoCode = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -21.577865, -45.448647 is " + reverseGeoCode.nearestPlace(-21.577865, -45.448647));
		
		ReverseGeoCode reverseGeoCode1 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -22.247189, -45.936803 is " + reverseGeoCode1.nearestPlace(-22.247189, -45.936803));
		
		ReverseGeoCode reverseGeoCode2 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -22.611901, -46.058236 is " + reverseGeoCode2.nearestPlace(-22.611901, -46.058236));
		
		ReverseGeoCode reverseGeoCode3 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.788953, -37.055833 is " + reverseGeoCode3.nearestPlace(-10.788953, -37.055833));
	
		ReverseGeoCode reverseGeoCode4 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.681840, -37.151071 is " + reverseGeoCode4.nearestPlace(-10.681840, -37.151071));
		
		ReverseGeoCode reverseGeoCode5 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.972179, -37.367780 is " + reverseGeoCode5.nearestPlace(-10.972179, -37.367780));
		
		ReverseGeoCode reverseGeoCode6 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -22.241441, -45.862386 is " + reverseGeoCode6.nearestPlace(-22.241441, -45.862386));
		
		ReverseGeoCode reverseGeoCode7 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -28.628962, -52.871157 is " + reverseGeoCode7.nearestPlace(-28.628962, -52.871157));
		
		ReverseGeoCode reverseGeoCode8 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.668058, -37.300347 is " + reverseGeoCode8.nearestPlace(-10.668058, -37.300347));
		
		ReverseGeoCode reverseGeoCode9 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.645833, -37.193889 is " + reverseGeoCode9.nearestPlace(-10.645833, -37.193889));
		
		ReverseGeoCode reverseGeoCode10 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.727778, -37.186944 is " + reverseGeoCode10.nearestPlace(-10.727778, -37.186944));
		
		ReverseGeoCode reverseGeoCode11 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.583978, -37.122397 is " + reverseGeoCode11.nearestPlace(-10.583978, -37.122397));
		
		ReverseGeoCode reverseGeoCode12 = new ReverseGeoCode(new FileInputStream("/home/vitor/BR.txt"), true);
		System.out.println("Nearest to -10.91, -37.07 is " + reverseGeoCode12.nearestPlace(-10.91, -37.07));

	}
}
